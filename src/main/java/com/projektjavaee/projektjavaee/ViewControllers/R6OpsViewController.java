package com.projektjavaee.projektjavaee.ViewControllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/operators")
public class R6OpsViewController {

    @RequestMapping("/all")
    public String allOps(){
        return "allOps";
    }

    @RequestMapping("/id")
    public String opsbyId(@RequestParam("id") int id) {
        return "opsById";
    }

    @RequestMapping("/edit")
    public String opEdit(@RequestParam("id") int id){
        return "opEdit";
    }

    @RequestMapping("/add")
    public String opAdd(){
        return "opAdd";
    }
}
