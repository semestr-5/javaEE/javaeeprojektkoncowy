package com.projektjavaee.projektjavaee.ViewControllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

@Controller
@RequestMapping("/")
public class HomeController {
    @RequestMapping("/")
    public RedirectView home(RedirectAttributes attributes){
        return new RedirectView("operators/all");
    }
}
