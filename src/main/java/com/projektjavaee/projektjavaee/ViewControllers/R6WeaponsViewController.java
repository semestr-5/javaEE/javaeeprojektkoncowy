package com.projektjavaee.projektjavaee.ViewControllers;

import com.projektjavaee.projektjavaee.DataService.R6Service;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/weapons")
public class R6WeaponsViewController {
    @RequestMapping("/all")
    public String allWeapons(){
        return "allWeapons";
    }

    @RequestMapping("/id")
    public String weaponsById(@RequestParam("id") int id) {
        return "weaponsById";
    }

    @RequestMapping("/edit")
    public String weaponEdit(@RequestParam("id") int id){
        return "weaponEdit";
    }

    @RequestMapping("/add")
    public String weaponAdd(){
        return "weaponAdd";
    }
}
