package com.projektjavaee.projektjavaee.ViewModels;

import com.projektjavaee.projektjavaee.Models.R6WeaponModel;

public class R6WeaponViewModel {
    private R6WeaponModel weapon;

    private String operatorsHTML;

    public R6WeaponViewModel(R6WeaponModel weapon, String operatorsHTML) {
        this.weapon = weapon;
        this.operatorsHTML = operatorsHTML;
    }

    public R6WeaponModel getWeapon() {
        return weapon;
    }

    public void setWeapon(R6WeaponModel weapon) {
        this.weapon = weapon;
    }

    public String getOperatorsHTML() {
        return operatorsHTML;
    }

    public void setOperatorsHTML(String operatorsHTML) {
        this.operatorsHTML = operatorsHTML;
    }
}
