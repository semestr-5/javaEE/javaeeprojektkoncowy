package com.projektjavaee.projektjavaee.ViewModels;

public class R6WeaponsAllViewModel {
    private int Id;

    private String name;

    private String type;

    public R6WeaponsAllViewModel(int weaponId, String name, String type) {
        this.Id = weaponId;
        this.name = name;
        this.type = type;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}