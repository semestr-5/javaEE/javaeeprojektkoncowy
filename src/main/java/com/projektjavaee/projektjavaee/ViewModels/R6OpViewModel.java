package com.projektjavaee.projektjavaee.ViewModels;

import com.projektjavaee.projektjavaee.Models.R6OpModel;

public class R6OpViewModel {
    private R6OpModel op;

    private String weaponsHTML;

    public R6OpViewModel(R6OpModel op, String weaponsHTML) {
        this.op = op;
        this.weaponsHTML = weaponsHTML;
    }

    public R6OpModel getOp() {
        return op;
    }

    public void setOp(R6OpModel op) {
        this.op = op;
    }

    public String getWeaponsHTML() {
        return weaponsHTML;
    }

    public void setWeaponsHTML(String weaponsHTML) {
        this.weaponsHTML = weaponsHTML;
    }
}
