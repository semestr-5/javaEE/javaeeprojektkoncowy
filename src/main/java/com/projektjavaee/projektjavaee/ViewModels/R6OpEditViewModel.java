package com.projektjavaee.projektjavaee.ViewModels;

import com.projektjavaee.projektjavaee.Models.R6OpModel;
import com.projektjavaee.projektjavaee.Models.R6WeaponModel;

import java.util.List;

public class R6OpEditViewModel {
    private R6OpModel op;
    private List<R6WeaponModel> weapons;

    public R6OpEditViewModel(R6OpModel op, List<R6WeaponModel> weapons) {
        this.op = op;
        this.weapons = weapons;
    }

    public R6OpModel getOp() {
        return op;
    }

    public void setOp(R6OpModel op) {
        this.op = op;
    }

    public List<R6WeaponModel> getWeapons() {
        return weapons;
    }

    public void setWeapons(List<R6WeaponModel> weapons) {
        this.weapons = weapons;
    }
}
