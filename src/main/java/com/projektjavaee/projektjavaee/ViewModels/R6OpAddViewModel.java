package com.projektjavaee.projektjavaee.ViewModels;

import com.projektjavaee.projektjavaee.Models.R6OpModel;

public class R6OpAddViewModel {
    private R6OpModel op;

    private int[] weaponsId;

    public R6OpAddViewModel(R6OpModel op, int[] weaponsId) {
        this.op = op;
        this.weaponsId = weaponsId;
    }

    public R6OpModel getOp() {
        return op;
    }

    public void setOp(R6OpModel op) {
        this.op = op;
    }

    public int[] getWeaponsId() {
        return weaponsId;
    }

    public void setWeaponsId(int[] weaponsId) {
        this.weaponsId = weaponsId;
    }
}
