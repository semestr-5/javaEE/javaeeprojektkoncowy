package com.projektjavaee.projektjavaee.Models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class R6WeaponModel {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "ID")
    private int weaponId;

    @Column(name = "Name")
    private String name;

    @Column(name = "Type")
    private String type;

    @Column(name = "Damage")
    private int damage;

    @Column(name = "DamageDrop")
    private int damageDrop;

    @Column(name = "DamageSuppresed")
    private int damageSuppresed;

    @Column(name = "RateOfFire")
    private int rateOfFire;

    @Column(name = "Mobility")
    private int mobility;

    @Column(name = "MagazineSize")
    private int magazineSize;

    @Column(name = "AmmoType")
    private String ammoType;

    @Column(name = "ImageUrl")
    private String imageUrl;

    @OneToMany(mappedBy = "weapon", cascade = CascadeType.ALL)
    @JsonIgnore
    Set<R6OperatorWeaponModel> operatorWeapons;

    public R6WeaponModel() {
    }

    public int getWeaponId() {
        return weaponId;
    }

    public void setWeaponId(int weaponId) {
        this.weaponId = weaponId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public int getDamageDrop() {
        return damageDrop;
    }

    public void setDamageDrop(int damageDrop) {
        this.damageDrop = damageDrop;
    }

    public int getDamageSuppresed() {
        return damageSuppresed;
    }

    public void setDamageSuppresed(int damageSuppresed) {
        this.damageSuppresed = damageSuppresed;
    }

    public int getRateOfFire() {
        return rateOfFire;
    }

    public void setRateOfFire(int rateOfFire) {
        this.rateOfFire = rateOfFire;
    }

    public int getMobility() {
        return mobility;
    }

    public void setMobility(int mobility) {
        this.mobility = mobility;
    }

    public int getMagazineSize() {
        return magazineSize;
    }

    public void setMagazineSize(int magazineSize) {
        this.magazineSize = magazineSize;
    }

    public String getAmmoType() {
        return ammoType;
    }

    public void setAmmoType(String ammoType) {
        this.ammoType = ammoType;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Set getOperatorWeapons() {
        return operatorWeapons;
    }

    public void setOperatorWeapons(Set operatorWeapons) {
        this.operatorWeapons = operatorWeapons;
    }
}
