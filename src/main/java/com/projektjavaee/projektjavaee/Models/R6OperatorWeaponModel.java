package com.projektjavaee.projektjavaee.Models;

import javax.persistence.*;

@Entity
public class R6OperatorWeaponModel {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "ID")
    private int linkId;

    @Column(name = "WeaponSlot")
    private int weaponSlot;

    @ManyToOne
    @JoinColumn(name="FK_R6OpModelId")
    private R6OpModel operator;

    @ManyToOne
    @JoinColumn(name="FK_R6WeaponModelId")
    private R6WeaponModel weapon;

    public R6OperatorWeaponModel() {
    }

    public R6OperatorWeaponModel(int weaponSlot, R6OpModel operator, R6WeaponModel weapon) {
        this.weaponSlot = weaponSlot;
        this.operator = operator;
        this.weapon = weapon;
    }

    public int getLinkId() {
        return linkId;
    }

    public void setLinkId(int linkId) {
        this.linkId = linkId;
    }

    public int getWeaponSlot() {
        return weaponSlot;
    }

    public void setWeaponSlot(int weaponSlot) {
        this.weaponSlot = weaponSlot;
    }

    public R6OpModel getOperator() {
        return operator;
    }

    public void setOperator(R6OpModel operator) {
        this.operator = operator;
    }

    public R6WeaponModel getWeapon() {
        return weapon;
    }

    public void setWeapon(R6WeaponModel weapon) {
        this.weapon = weapon;
    }
}
