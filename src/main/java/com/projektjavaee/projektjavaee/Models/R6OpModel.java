package com.projektjavaee.projektjavaee.Models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class R6OpModel {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "ID")
    private int operatorId;

    @Column(name = "FirstName")
    private String firstName;

    @Column(name = "LastName")
    private String lastName;

    @Column(name = "Nick")
    private String nick;

    @Column(name = "Description")
    private String description;

    @Column(name = "BirthDate")
    private String birthDate;

    @Column(name = "Position")
    private String position;

    @Column(name = "BirthPlaceHTML")
    private String birthPlaceHTML;

    @Column(name = "Organization")
    private String organization;

    @Column(name = "Height")
    private float height;

    @Column(name = "Weight")
    private float weight;

    @Column(name = "ArmorRating")
    private int armorRating;

    @Column(name = "SpeedRating")
    private int speedRating;

    @Column(name = "MainImageUrl")
    private String mainImageUrl;

    @Column(name = "LogoUrl")
    private String LogoUrl;

    @OneToMany(mappedBy = "operator", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JsonIgnore
    private Set<R6OperatorWeaponModel> operatorWeapons;

    public R6OpModel() {
    }

    public int getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(int operatorId) {
        this.operatorId = operatorId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getBirthPlaceHTML() {
        return birthPlaceHTML;
    }

    public void setBirthPlaceHTML(String birthPlaceHTML) {
        this.birthPlaceHTML = birthPlaceHTML;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public int getArmorRating() {
        return armorRating;
    }

    public void setArmorRating(int armorRating) {
        this.armorRating = armorRating;
    }

    public int getSpeedRating() {
        return speedRating;
    }

    public void setSpeedRating(int speedRating) {
        this.speedRating = speedRating;
    }

    public String getMainImageUrl() {
        return mainImageUrl;
    }

    public void setMainImageUrl(String mainImageUrl) {
        this.mainImageUrl = mainImageUrl;
    }

    public String getLogoUrl() {
        return LogoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        LogoUrl = logoUrl;
    }

    public Set getOperatorWeapons() {
        return operatorWeapons;
    }

    public void setOperatorWeapons(Set operatorWeapons) {
        this.operatorWeapons = operatorWeapons;
    }
}
