package com.projektjavaee.projektjavaee.Repositories;

import com.projektjavaee.projektjavaee.Models.R6OpModel;
import com.projektjavaee.projektjavaee.Models.R6WeaponModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface R6WeaponsRepository extends CrudRepository<R6WeaponModel, Long> {
    List<R6WeaponModel> findAll();
    R6WeaponModel findByWeaponId(int id);
    void deleteByWeaponId(int id);
}
