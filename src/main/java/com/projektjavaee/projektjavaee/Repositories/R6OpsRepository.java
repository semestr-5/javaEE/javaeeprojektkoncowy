package com.projektjavaee.projektjavaee.Repositories;

import com.projektjavaee.projektjavaee.Models.R6OpModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Id;
import java.util.List;

@Repository
public interface R6OpsRepository extends CrudRepository<R6OpModel, Long> {
    List<R6OpModel> findAll();
    R6OpModel findByOperatorId(int operatorId);
    void deleteByOperatorId(int id);
    List<R6OpModel> findByNickContaining(String nick); // LIKE '%nick%'
    List<R6OpModel> findByNickStartingWith(String nick); // LIKE 'nick%'
    List<R6OpModel> findByNickEndingWith(String nick); //LIKE '%nick'
}
