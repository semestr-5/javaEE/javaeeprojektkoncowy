package com.projektjavaee.projektjavaee.Repositories;

import com.projektjavaee.projektjavaee.Models.R6OpModel;
import com.projektjavaee.projektjavaee.Models.R6OperatorWeaponModel;
import com.projektjavaee.projektjavaee.Models.R6WeaponModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface R6OperatorWeaponsRepository extends CrudRepository<R6OperatorWeaponModel, Long> {
    List<R6OperatorWeaponModel> findAll();
}
