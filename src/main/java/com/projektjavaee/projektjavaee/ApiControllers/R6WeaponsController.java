package com.projektjavaee.projektjavaee.ApiControllers;

import com.projektjavaee.projektjavaee.DataService.R6Service;
import com.projektjavaee.projektjavaee.Models.R6OpModel;
import com.projektjavaee.projektjavaee.Models.R6WeaponModel;
import com.projektjavaee.projektjavaee.ViewModels.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/weapons")
public class R6WeaponsController {
    @Autowired
    R6Service r6Service;

    @GetMapping(value = "/all", produces = "application/json")
    public List<R6WeaponsAllViewModel> getAll(){
        return r6Service.getAllWeaponsList();
    }

    @GetMapping(value = "/id", produces = "application/json")
    public R6WeaponViewModel getById(@RequestParam("id") int id){ return r6Service.getWeaponById(id); }

    @RequestMapping(value = "/delete", method = RequestMethod.DELETE, produces = "application/json")
    public void deleteById(@RequestParam("id") int id){
        r6Service.deleteWeapon(id);
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String add(@RequestBody R6WeaponModel model){
        r6Service.addWeapon(model);

        return "{ \"Result\": \"Success\" }";
    }

    @RequestMapping(value = "/edit", method = RequestMethod.PUT)
    public String edit(@RequestBody R6WeaponModel model){
        r6Service.editWeapon(model);

        return "{ \"Result\": \"Success\" }";
    }
}
