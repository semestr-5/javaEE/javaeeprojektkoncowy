package com.projektjavaee.projektjavaee.ApiControllers;

import com.projektjavaee.projektjavaee.DataService.R6Service;
import com.projektjavaee.projektjavaee.Models.R6OpModel;
import com.projektjavaee.projektjavaee.Models.R6WeaponModel;
import com.projektjavaee.projektjavaee.ViewModels.R6OpAddViewModel;
import com.projektjavaee.projektjavaee.ViewModels.R6OpEditViewModel;
import com.projektjavaee.projektjavaee.ViewModels.R6OpViewModel;
import com.projektjavaee.projektjavaee.ViewModels.R6OpsAllViewModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/operators")
public class R6OpsController {
    @Autowired
    R6Service r6Service;

    @GetMapping(value = "/all", produces = "application/json")
    public List<R6OpsAllViewModel> getAll() {
        return r6Service.getAllOpsList();
    }

    @GetMapping(value = "/like", produces = "application/json")
    public List<R6OpModel> getById(@RequestParam("ths") String ths) {
        return r6Service.getOpsLike(ths);
    }

    @GetMapping(value = "/id", produces = "application/json")
    public R6OpViewModel getById(@RequestParam("id") int id) {
        return r6Service.getOpById(id);
    }

    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public void deleteById(@RequestParam("id") int id) {
        r6Service.deleteOp(id);
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String add(@RequestBody R6OpAddViewModel model) {
        r6Service.addOp(model);

        return "{ \"Result\": \"Success\" }";
    }

    @RequestMapping(value = "/edit", method = RequestMethod.PUT)
    public String edit(@RequestBody R6OpAddViewModel model){
        r6Service.addOp(model);

        return "{ \"Result\": \"Success\" }";
    }

    @GetMapping(value = "/edit", produces = "application/json")
    public R6OpEditViewModel getEditById(@RequestParam("id") int id) {
        return r6Service.getOpEditById(id);
    }
}
