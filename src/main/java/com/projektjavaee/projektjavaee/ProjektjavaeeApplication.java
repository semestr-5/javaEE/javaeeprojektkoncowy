package com.projektjavaee.projektjavaee;

import com.projektjavaee.projektjavaee.DataService.R6Service;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication
@EntityScan("com.projektjavaee.projektjavaee.Models")
public class ProjektjavaeeApplication {
	public static void main(String[] args) {
		SpringApplication.run(ProjektjavaeeApplication.class, args);
	}

}

