package com.projektjavaee.projektjavaee.DataService;

import com.projektjavaee.projektjavaee.Models.R6OpModel;
import com.projektjavaee.projektjavaee.Models.R6WeaponModel;
import com.projektjavaee.projektjavaee.ViewModels.*;

import java.util.List;

public interface IR6Service {
    List<R6OpModel> getAllOps();

    List<R6OpModel> getOpsLike(String like);

    List<R6OpsAllViewModel> getAllOpsList();

    List<R6WeaponsAllViewModel> getAllWeaponsList();

    R6OpViewModel getOpById(int id);

    R6WeaponViewModel getWeaponById(int id);

    R6OpEditViewModel getOpEditById(int id);

    void addOp(R6OpAddViewModel model);

    void addWeapon(R6WeaponModel model);

    void editWeapon(R6WeaponModel model);

    void deleteOp(int id);

    void deleteWeapon(int id);
}
