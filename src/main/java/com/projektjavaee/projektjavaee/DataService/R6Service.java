package com.projektjavaee.projektjavaee.DataService;

import com.projektjavaee.projektjavaee.Models.R6OpModel;
import com.projektjavaee.projektjavaee.Models.R6OperatorWeaponModel;
import com.projektjavaee.projektjavaee.Models.R6WeaponModel;
import com.projektjavaee.projektjavaee.Repositories.R6OperatorWeaponsRepository;
import com.projektjavaee.projektjavaee.Repositories.R6OpsRepository;
import com.projektjavaee.projektjavaee.Repositories.R6WeaponsRepository;
import com.projektjavaee.projektjavaee.ViewModels.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.LockModeType;
import javax.transaction.TransactionScoped;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class R6Service implements IR6Service {
    @Autowired
    private R6OpsRepository r6OpsRepository;

    @Autowired
    private R6OperatorWeaponsRepository r6OperatorWeaponsRepository;

    @Autowired
    private R6WeaponsRepository r6WeaponsRepository;

    public List<R6OpModel> getAllOps(){
        return r6OpsRepository.findAll();
    }

    public List<R6OpModel> getOpsLike(String like){
        return  r6OpsRepository.findByNickContaining(like);
    }

    public List<R6OpsAllViewModel> getAllOpsList(){
        List<R6OpsAllViewModel> list = new ArrayList<R6OpsAllViewModel>();

        List<R6OpModel> ops = r6OpsRepository.findAll();

        for (R6OpModel op : ops ) {
            list.add(new R6OpsAllViewModel(
                    op.getOperatorId(),
                    op.getFirstName(),
                    op.getLastName(),
                    op.getNick(),
                    op.getLogoUrl()));
        }

        return list;
    }

    public List<R6WeaponsAllViewModel> getAllWeaponsList(){
        List<R6WeaponsAllViewModel> list = new ArrayList<R6WeaponsAllViewModel>();

        List<R6WeaponModel> weapons = r6WeaponsRepository.findAll();

        for (R6WeaponModel weapon : weapons ) {
            list.add(new R6WeaponsAllViewModel(
                    weapon.getWeaponId(),
                    weapon.getName(),
                    weapon.getType()));
        }

        return list;
    }

    public R6OpViewModel getOpById(int id){
        R6OpModel op = r6OpsRepository.findByOperatorId(id);
        List<R6OperatorWeaponModel> list = new ArrayList<R6OperatorWeaponModel>(op.getOperatorWeapons());
        String temp = "";

        if (!list.isEmpty())
            for (R6OperatorWeaponModel item: list) {
                R6WeaponModel tempweap = item.getWeapon();
                temp += "<a style=\"margin-left:10px; margin-right: 10px;\" href=\"http://localhost:8080/weapons/id?id="+tempweap.getWeaponId()+"\">" + tempweap.getName() + "</a>";
            }

        return new R6OpViewModel(op, temp);
    }

    public R6WeaponViewModel getWeaponById(int id){
        R6WeaponModel weapon = r6WeaponsRepository.findByWeaponId(id);
        List<R6OperatorWeaponModel> list = new ArrayList<R6OperatorWeaponModel>(weapon.getOperatorWeapons());
        String temp = "";

        if (!list.isEmpty())
            for (R6OperatorWeaponModel item: list) {
                R6OpModel tempop =  item.getOperator();
                temp += "<a style=\"margin-left:10px; margin-right: 10px;\" href=\"http://localhost:8080/operators/id?id="+tempop.getOperatorId()+"\"><img width=\"50\" height=\"50\" src=\"" + tempop.getLogoUrl() + "\"/></a>";
            }

        return new R6WeaponViewModel(weapon, temp);
    }

    public R6OpEditViewModel getOpEditById(int id){
        R6OpModel op = r6OpsRepository.findByOperatorId(id);
        List<R6OperatorWeaponModel> list = new ArrayList<R6OperatorWeaponModel>(op.getOperatorWeapons());
        List<R6WeaponModel> temp = new ArrayList<R6WeaponModel>();

        if (!list.isEmpty())
            for (R6OperatorWeaponModel item: list) {
                temp.add(item.getWeapon());
            }

        return new R6OpEditViewModel(op, temp);
    }

    public void addOp(R6OpAddViewModel model){
        R6OpModel m = model.getOp();
        Set set1 = new HashSet();
        List<R6WeaponModel> list = new ArrayList<R6WeaponModel>();
        for (int id:model.getWeaponsId()) {
            R6WeaponModel weapon = r6WeaponsRepository.findByWeaponId(id);
            R6OperatorWeaponModel opweapon = new R6OperatorWeaponModel(1, m, weapon);

            Set set2 = new HashSet();
            set1.add(opweapon);
            set2.add(opweapon);
            weapon.setOperatorWeapons(set2);
            list.add(weapon);
        }

        m.setOperatorWeapons(set1);
        saveOp(m);
        saveWeapons(list);
    }

    @Transactional
    public void saveOp(R6OpModel m){
        r6OpsRepository.save(m);
    }

    @Transactional
    public void saveWeapons( List<R6WeaponModel> list){
        r6WeaponsRepository.saveAll(list);
    }

    @Transactional
    public void addWeapon(R6WeaponModel model){
        r6WeaponsRepository.save(model);
    }

    @Transactional
    public void editWeapon(R6WeaponModel model){
        r6WeaponsRepository.save(model);
    }

    @Transactional
    public void deleteOp(int id){
        r6OpsRepository.deleteByOperatorId(id);
    }

    @Transactional
    public void deleteWeapon(int id){
        r6WeaponsRepository.deleteByWeaponId(id);
    }

    @EventListener
    @Transactional
    public void Seed(ContextRefreshedEvent event){
        R6OpModel ash = new R6OpModel();
        ash.setArmorRating(1);
        ash.setBirthDate("1983-12-24");
        ash.setBirthPlaceHTML("<a href=\"https://vignette.wikia.nocookie.net/rainbowsix/images/d/d4/Flag_of_Israel.svg/revision/latest?cb=20130424204644\" class=\"image image-thumbnail\"><img src=\"https://vignette.wikia.nocookie.net/rainbowsix/images/d/d4/Flag_of_Israel.svg/revision/latest/scale-to-width-down/20?cb=20130424204644\" alt=\"Flag of Israel\" class=\"lzyPlcHld lzyTrns lzyLoaded\" data-image-key=\"Flag_of_Israel.svg\" data-image-name=\"Flag of Israel.svg\" data-src=\"https://vignette.wikia.nocookie.net/rainbowsix/images/d/d4/Flag_of_Israel.svg/revision/latest/scale-to-width-down/20?cb=20130424204644\" width=\"20\" height=\"15\" onload=\"if(typeof ImgLzy==='object'){ImgLzy.load(this)}\"><noscript><img src=\"https://vignette.wikia.nocookie.net/rainbowsix/images/d/d4/Flag_of_Israel.svg/revision/latest/scale-to-width-down/20?cb=20130424204644\" \t alt=\"Flag of Israel\"  \tclass=\"\" \t \tdata-image-key=\"Flag_of_Israel.svg\" \tdata-image-name=\"Flag of Israel.svg\" \t \t width=\"20\"  \t height=\"15\"  \t \t \t \t></noscript></a> <a href=\"http://en.wikipedia.org/wiki/Jerusalem\" class=\"extiw\" title=\"wikipedia:Jerusalem\">Jerusalem</a>, <a href=\"http://en.wikipedia.org/wiki/Israel\" class=\"extiw\" title=\"wikipedia:Israel\">Israel</a>");
        ash.setDescription("");
        ash.setFirstName("Eliza");
        ash.setHeight(170);
        ash.setLastName("Cohen");
        ash.setLogoUrl("https://vignette.wikia.nocookie.net/rainbowsix/images/d/d7/Ash_Icon_-_Standard.png/revision/latest?cb=20151222045522");
        ash.setMainImageUrl("https://vignette.wikia.nocookie.net/rainbowsix/images/4/42/Ash_-_Full_Body.png/revision/latest/scale-to-width-down/173?cb=20180224040849");
        ash.setNick("Ash");
        ash.setOrganization("FBI SWAT");
        ash.setPosition("Attacker");
        ash.setSpeedRating(3);
        ash.setWeight(63);

        R6WeaponModel g36c = new R6WeaponModel();
        g36c.setAmmoType("5.56×45mm NATO");
        g36c.setDamage(38);
        g36c.setDamageDrop(10);
        g36c.setDamageSuppresed(32);
        g36c.setImageUrl("https://vignette.wikia.nocookie.net/rainbowsix/images/8/8d/G36c-image.jpg/revision/latest/scale-to-width-down/309?cb=20151209024346");
        g36c.setMagazineSize(31);
        g36c.setMobility(50);
        g36c.setName("G36C");
        g36c.setRateOfFire(780);
        g36c.setType("Assault Rifle");

        R6WeaponModel r4c = new R6WeaponModel();
        r4c.setAmmoType("5.56×45mm NATO");
        r4c.setDamage(39);
        r4c.setDamageDrop(14);
        r4c.setDamageSuppresed(36);
        r4c.setImageUrl("https://vignette.wikia.nocookie.net/rainbowsix/images/8/8d/G36c-image.jpg/revision/latest/scale-to-width-down/309?cb=20151209024346");
        r4c.setMagazineSize(31);
        r4c.setMobility(50);
        r4c.setName("R4-C");
        r4c.setRateOfFire(860);
        r4c.setType("Assault Rifle");


        R6OpModel thermite = new R6OpModel();
        thermite.setArmorRating(2);
        thermite.setBirthDate("1982-03-14");
        thermite.setBirthPlaceHTML("<a href=\"https://vignette.wikia.nocookie.net/rainbowsix/images/3/3a/U.S_Flag.png/revision/latest?cb=20140122175605\" class=\"image image-thumbnail\"><img src=\"https://vignette.wikia.nocookie.net/rainbowsix/images/3/3a/U.S_Flag.png/revision/latest?cb=20140122175605\" alt=\"U.S Flag\" class=\"lzyPlcHld lzyTrns lzyLoaded\" data-image-key=\"U.S_Flag.png\" data-image-name=\"U.S Flag.png\" data-src=\"https://vignette.wikia.nocookie.net/rainbowsix/images/3/3a/U.S_Flag.png/revision/latest?cb=20140122175605\" width=\"23\" height=\"12\" onload=\"if(typeof ImgLzy==='object'){ImgLzy.load(this)}\"><noscript><img src=\"https://vignette.wikia.nocookie.net/rainbowsix/images/3/3a/U.S_Flag.png/revision/latest?cb=20140122175605\" \t alt=\"U.S Flag\"  \tclass=\"\" \t \tdata-image-key=\"U.S_Flag.png\" \tdata-image-name=\"U.S Flag.png\" \t \t width=\"23\"  \t height=\"12\"  \t \t \t \t></noscript></a> <a href=\"http://en.wikipedia.org/wiki/Plano\" class=\"extiw\" title=\"wikipedia:Plano\">Plano</a>, <a href=\"http://en.wikipedia.org/wiki/Texas\" class=\"extiw\" title=\"wikipedia:Texas\">Texas</a>");
        thermite.setDescription("");
        thermite.setFirstName("Jordan");
        thermite.setHeight(178);
        thermite.setLastName("Trace");
        thermite.setLogoUrl("https://vignette.wikia.nocookie.net/rainbowsix/images/e/e3/Thermite_Badge_2.png/revision/latest?cb=20151222045527");
        thermite.setMainImageUrl("https://vignette.wikia.nocookie.net/rainbowsix/images/7/78/Large-thermite.e973bb04.png/revision/latest/scale-to-width-down/300?cb=20171224002441");
        thermite.setNick("Thermite");
        thermite.setOrganization("FBI SWAT");
        thermite.setPosition("Attacker");
        thermite.setSpeedRating(2);
        thermite.setWeight(80);

        Set set1 = new HashSet();
        Set set2 = new HashSet();
        Set set3 = new HashSet();

        R6OperatorWeaponModel bind1 = new R6OperatorWeaponModel();
        bind1.setOperator(ash);
        bind1.setWeapon(g36c);
        bind1.setWeaponSlot(1);
        set1.add(bind1);
        set2.add(bind1);

        R6OperatorWeaponModel bind2 = new R6OperatorWeaponModel();
        bind2.setOperator(ash);
        bind2.setWeapon(r4c);
        bind2.setWeaponSlot(1);
        set1.add(bind2);
        set3.add(bind2);

        ash.setOperatorWeapons(set1);
        g36c.setOperatorWeapons(set2);
        r4c.setOperatorWeapons(set3);

        g36c = r6WeaponsRepository.save(g36c);
        r4c = r6WeaponsRepository.save(r4c);
        ash = r6OpsRepository.save(ash);
        thermite = r6OpsRepository.save(thermite);
    }
}
