CREATE TABLE R6Weapons(
  ID INTEGER PRIMARY KEY AUTOINCREMENT,
  Name nvarchar(50),
  Type nvarchar(50),
  Damage int,
  DamageDrop int,
  DamageSupressed int,
  RateOfFire int,
  Mobility int,
  MagazineSize int,
  AmmoType nvarchar(50),
  ImageUrl nvarchar(150)
);