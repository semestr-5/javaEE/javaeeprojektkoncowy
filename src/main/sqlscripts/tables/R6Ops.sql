CREATE TABLE R6Ops (
  ID INTEGER PRIMARY KEY AUTOINCREMENT,
  FirstName nvarchar(50),
  LastName nvarchar(50),
  Nick nvarchar(50),
  Description nvarchar(50),
  BirthDate datetime,
  Position INTEGER,
  BirthPlaceHTML nvarchar(50),
  Organization nvarchar(50),
  Height float,
  Weight float,
  ArmorRating int,
  SpeedRating int,
  MainImageUrl nvarchar(150),
  LogoUrl nvarchar(150)
);