INSERT INTO R6Ops (FirstName, LastName, Nick, Description, BirthDate, Position, BirthPlaceHTML, Organization, Height, Weight, ArmorRating, SpeedRating, MainImageUrl, LogoUrl)
    VALUES ('Eliza', 'Cohen', 'Ash', '', '1983-12-24', 'Attacker', 'Jerusalem, Israel', 'FBI SWAT', 170, 63, 1, 3,
            'https://vignette.wikia.nocookie.net/rainbowsix/images/4/42/Ash_-_Full_Body.png/revision/latest/scale-to-width-down/173?cb=20180224040849',
            'https://vignette.wikia.nocookie.net/rainbowsix/images/d/d7/Ash_Icon_-_Standard.png/revision/latest?cb=20151222045522');

INSERT INTO R6Weapons (Name, Type, Damage, DamageDrop, DamageSupressed, RateOfFire, Mobility, MagazineSize, AmmoType, ImageUrl)
VALUES ('G36C', 'Assault Rifle', 38, 10, 32, 780, 50, 31, '5.56×45mm NATO',
        'https://vignette.wikia.nocookie.net/rainbowsix/images/8/8d/G36c-image.jpg/revision/latest/scale-to-width-down/309?cb=20151209024346');

INSERT INTO R6OperatorWeapons (WeaponID, OperatorID, WeaponSlot)
VALUES (1, 1, 1);

INSERT INTO R6Weapons (Name, Type, Damage, DamageDrop, DamageSupressed, RateOfFire, Mobility, MagazineSize, AmmoType, ImageUrl)
VALUES ('R4-C', 'Assault Rifle', 39, 14, 36, 860, 50, 31, '5.56×45mm NATO',
        'https://vignette.wikia.nocookie.net/rainbowsix/images/8/8d/G36c-image.jpg/revision/latest/scale-to-width-down/309?cb=20151209024346');
INSERT INTO R6OperatorWeapons (WeaponID, OperatorID, WeaponSlot)
VALUES (2, 1, 1);
