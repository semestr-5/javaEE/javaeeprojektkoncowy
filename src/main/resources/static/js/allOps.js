function getAllOps() {
    $.ajax({
        url: "http://localhost:8080/api/operators/all",
    }).done(function (data) {
        data.forEach(function (item) {
            var row = document.createElement("tr");
            var col = document.createElement("td");
            var aicon = document.createElement("a");
            var icon = document.createElement("img");
            aicon.href = "/operators/id?id=" + item.id;
            icon.src = item.logoUrl;
            icon.width=50;
            icon.height=50;
            aicon.appendChild(icon);
            col.appendChild(aicon);
            row.appendChild(col);
            col = document.createElement("td");
            col.innerText
                = item.firstName;
            row.appendChild(col);
            col = document.createElement("td");
            col.innerText
                = item.lastName;
            row.appendChild(col);
            col = document.createElement("td");
            col.innerText
                = item.nick;
            row.appendChild(col);

            col = document.createElement("td");
            var btn = document.createElement("a");
            btn.innerText="Edit";
            btn.classList.add("btn");
            btn.classList.add("btn-default");
            btn.href = "/operators/edit?id=" + item.id;
            col.appendChild(btn);
            row.appendChild(col);

            col = document.createElement("td");
            btn = document.createElement("a");
            btn.innerText="Delete";
            btn.classList.add("btn");
            btn.classList.add("btn-default")
            btn.addEventListener("click",function(){
                console.log(item.id);
                deleteOp(item.id);
            });
            col.appendChild(btn);
            row.appendChild(col);

            document.getElementById("operators")
                .appendChild(row);
        });
    });
}

function deleteOp(id){
    $.ajax({
        url: "http://localhost:8080/api/operators/delete?id="+id,
        method:"DELETE",
    }).done(function() {
        location.reload();
    })
}

jQuery(document).ready(function($) {
    getAllOps();
});