jQuery(document).ready(function($) {
    init();
    getOpById();
});

function init() {
    var submit_btn = document.getElementById("submit");
    submit_btn.addEventListener("click",function(event){
        event.preventDefault();
        var data = {};
        data.op={};
        data.op.firstName = document.getElementById("firstname").value;
        data.op.lastName = document.getElementById("lastname").value;
        data.op.nick = document.getElementById("nick").value;
        data.op.description = document.getElementById("description").value;
        data.op.birthDate = document.getElementById("birthdate").value;
        data.op.birthPlaceHTML = document.getElementById("birthplaceHTML").value;
        data.op.organization = document.getElementById("organization").value;
        data.op.height = document.getElementById("height").value;
        data.op.weight = document.getElementById("weight").value;
        data.op.armorRating = document.getElementById("armorrating").value;
        data.op.speedRating = document.getElementById("speedrating").value;
        data.op.mainImageUrl = document.getElementById("mainimgurl").value;
        data.op.logoUrl = document.getElementById("logourl").value;
        data.op.position = document.getElementById("position").value;
        data.op.operatorId = document.getElementById("id").value;
        data.weaponsId = getSelectValues();

        $.ajax({
            url: "http://localhost:8080/api/operators/edit",
            method:"PUT",
            contentType: 'application/json',
            dataType: 'json',
            data: JSON.stringify(data)
        }).done(function() {
            location.href = "http://localhost:8080/operators/all";
        });
    });
}

function getSelectValues() {
    var select = document.getElementById("s_weapons");
    var result = [];
    var options = select && select.options;
    var opt;

    for (var i=0, iLen=options.length; i<iLen; i++) {
        opt = options[i];

        if (opt.selected) {
            result.push(opt.value);
        }
    }
    return result;
}

function getOpById() {
    var id = getParameterByName('id');
    $.ajax({
        url: "http://localhost:8080/api/operators/edit?id="+id,
    }).done(function (data) {
        document.getElementById("firstname").value = data.op.firstName;
        document.getElementById("lastname").value = data.op.lastName;
        document.getElementById("nick").value = data.op.nick;
        document.getElementById("description").value = data.op.description;
        document.getElementById("birthdate").value = data.op.birthDate;
        document.getElementById("birthplaceHTML").value = data.op.birthPlaceHTML;
        document.getElementById("organization").value = data.op.organization;
        document.getElementById("height").value = data.op.height;
        document.getElementById("weight").value = data.op.weight;
        document.getElementById("armorrating").value = data.op.armorRating;
        document.getElementById("speedrating").value = data.op.speedRating;
        document.getElementById("mainimgurl").value = data.op.mainImageUrl;
        document.getElementById("logourl").value = data.op.logoUrl;
        document.getElementById("position").value = data.op.position;
        document.getElementById("id").value = id;

        var select = document.getElementById("s_weapons");

        $.ajax({
            url: "http://localhost:8080/api/weapons/all",
        }).done(function (data2) {
            var select = document.getElementById("s_weapons");
            data2.forEach(function (item) {
                var flaga = false;
                var option = document.createElement("option");
                option.value=item.id;
                option.innerText=item.name;

                data.weapons.forEach(function (item2) {
                    if (item.id==item2.weaponId){
                        flaga=true;
                    }
                });
                if (flaga==true){
                    option.selected=true;
                }
                select.appendChild(option);
            });
        });
    });
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}