function getOpById() {
    var id = getParameterByName('id');
    $.ajax({
        url: "http://localhost:8080/api/operators/id?id="+id,
    }).done(function (data) {
        var table = document.getElementById("op");
        var row = document.createElement("tr");
        var col = document.createElement("td");
        var img = document.createElement("img");

        col.colSpan = 2;
        img.src=data.op.logoUrl;
        img.width=50;
        img.height=50;
//1
        col.appendChild(img);
        row.appendChild(col);

        col = document.createElement("td");
        img = document.createElement("img");
        img.src=data.op.mainImageUrl;
        col.rowSpan = 13;
        col.style.width="40%";

        col.appendChild(img);
        row.appendChild(col);
        table.appendChild(row);

        row = document.createElement("tr");
        col = document.createElement("td");
//2
        col.innerText = "First Name";
        col.style.width="30%";
        row.appendChild(col);
        col = document.createElement("td");
        col.innerText = data.op.firstName;
        col.style.width="30%";

        row.appendChild(col);
        table.appendChild(row);

        row = document.createElement("tr");
        col = document.createElement("td");
//3
        col.innerText = "Last Name";
        col.style.width="30%";
        row.appendChild(col);
        col = document.createElement("td");
        col.innerText = data.op.lastName;
        col.style.width="30%";

        row.appendChild(col);
        table.appendChild(row);

        row = document.createElement("tr");
        col = document.createElement("td");
//4
        col.innerText = "Nickname";
        col.style.width="30%";
        row.appendChild(col);
        col = document.createElement("td");
        col.innerText = data.op.nick;
        col.style.width="30%";

        row.appendChild(col);
        table.appendChild(row);

        row = document.createElement("tr");
        col = document.createElement("td");
//5
        col.innerText = "Birthdate";
        col.style.width="30%";
        row.appendChild(col);
        col = document.createElement("td");
        col.innerText = data.op.birthDate;
        col.style.width="30%";

        row.appendChild(col);
        table.appendChild(row);

        row = document.createElement("tr");
        col = document.createElement("td");
//6
        col.innerText = "Birth Place";
        col.style.width="30%";
        row.appendChild(col);
        col = document.createElement("td");
        col.innerHTML = data.op.birthPlaceHTML;
        col.style.width="30%";

        row.appendChild(col);
        table.appendChild(row);

        row = document.createElement("tr");
        col = document.createElement("td");
//7
        col.innerText = "Position";
        col.style.width="30%";
        row.appendChild(col);
        col = document.createElement("td");
        col.innerText = data.op.position;
        col.style.width="30%";

        row.appendChild(col);
        table.appendChild(row);

        row = document.createElement("tr");
        col = document.createElement("td");
//8
        col.innerText = "Organization";
        col.style.width="30%";
        row.appendChild(col);
        col = document.createElement("td");
        col.innerText = data.op.organization;
        col.style.width="30%";

        row.appendChild(col);
        table.appendChild(row);

        row = document.createElement("tr");
        col = document.createElement("td");
//9
        col.innerText = "Height";
        col.style.width="30%";
        row.appendChild(col);
        col = document.createElement("td");
        col.innerText = data.op.height + "cm";
        col.style.width="30%";

        row.appendChild(col);
        table.appendChild(row);

        row = document.createElement("tr");
        col = document.createElement("td");
//10
        col.innerText = "Weight";
        col.style.width="30%";
        row.appendChild(col);
        col = document.createElement("td");
        col.innerText = data.op.weight + "kg";
        col.style.width="30%";

        row.appendChild(col);
        table.appendChild(row);

        row = document.createElement("tr");
        col = document.createElement("td");
//11
        col.innerText = "Armor Rating";
        col.style.width="30%";
        row.appendChild(col);
        col = document.createElement("td");
        col.innerText = data.op.armorRating;
        col.style.width="30%";

        row.appendChild(col);
        table.appendChild(row);

        row = document.createElement("tr");
        col = document.createElement("td");
//12
        col.innerText = "Speed Rating";
        row.appendChild(col);
        col = document.createElement("td");
        col.innerText = data.op.speedRating;

        row.appendChild(col);
        table.appendChild(row);

        row = document.createElement("tr");
        col = document.createElement("td");
//13
        col.innerText = "Weapons";
        row.appendChild(col);
        col = document.createElement("td");
        col.innerHTML = data.weaponsHTML;

        row.appendChild(col);
        table.appendChild(row);
    });
}

jQuery(document).ready(function($) {
    getOpById();
});

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}