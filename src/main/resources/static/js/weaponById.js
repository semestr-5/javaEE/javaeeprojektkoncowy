function getWeaponById() {
    var id = getParameterByName('id');
    $.ajax({
        url: "http://localhost:8080/api/weapons/id?id="+id,
    }).done(function (data) {
        var table = document.getElementById("weapon");
        var row = document.createElement("tr");
        var col = document.createElement("td");
        var img = document.createElement("img");
        col.innerText = "Name";
        col.style.width="30%";
        row.appendChild(col);
        col = document.createElement("td");
        col.innerText = data.weapon.name;
        col.style.width="30%";
        row.appendChild(col);
        col = document.createElement("td");
        col.rowSpan = 9;
        img.src=data.weapon.imageUrl;
        //img.width=50;
        //img.height=50;

        col.appendChild(img);
        row.appendChild(col);
        table.appendChild(row);

        row = document.createElement("tr");
        col = document.createElement("td");
//3
        col.innerText = "Type";
        col.style.width="30%";
        row.appendChild(col);
        col = document.createElement("td");
        col.innerText = data.weapon.type;
        col.style.width="30%";

        row.appendChild(col);
        table.appendChild(row);

        row = document.createElement("tr");
        col = document.createElement("td");
//4
        col.innerText = "Damage";
        col.style.width="30%";
        row.appendChild(col);
        col = document.createElement("td");
        col.innerText = data.weapon.damage;
        col.style.width="30%";

        row.appendChild(col);
        table.appendChild(row);

        row = document.createElement("tr");
        col = document.createElement("td");
//5
        col.innerText = "Damage Drop";
        col.style.width="30%";
        row.appendChild(col);
        col = document.createElement("td");
        col.innerText = data.weapon.damageDrop;
        col.style.width="30%";

        row.appendChild(col);
        table.appendChild(row);

        row = document.createElement("tr");
        col = document.createElement("td");
//6
        col.innerText = "Damage Suppresed";
        col.style.width="30%";
        row.appendChild(col);
        col = document.createElement("td");
        col.innerHTML = data.weapon.damageSuppresed;
        col.style.width="30%";

        row.appendChild(col);
        table.appendChild(row);

        row = document.createElement("tr");
        col = document.createElement("td");
//7
        col.innerText = "Rate of Fire";
        col.style.width="30%";
        row.appendChild(col);
        col = document.createElement("td");
        col.innerText = data.weapon.rateOfFire;
        col.style.width="30%";

        row.appendChild(col);
        table.appendChild(row);

        row = document.createElement("tr");
        col = document.createElement("td");
//8
        col.innerText = "Mobility";
        col.style.width="30%";
        row.appendChild(col);
        col = document.createElement("td");
        col.innerText = data.weapon.mobility;
        col.style.width="30%";

        row.appendChild(col);
        table.appendChild(row);

        row = document.createElement("tr");
        col = document.createElement("td");
//9
        col.innerText = "Magazine Size";
        col.style.width="30%";
        row.appendChild(col);
        col = document.createElement("td");
        col.innerText = data.weapon.magazineSize;
        col.style.width="30%";

        row.appendChild(col);
        table.appendChild(row);

        row = document.createElement("tr");
        col = document.createElement("td");
//10
        col.innerText = "Ammo Type";
        col.style.width="30%";
        row.appendChild(col);
        col = document.createElement("td");
        col.innerText = data.weapon.ammoType;
        col.style.width="30%";

        row.appendChild(col);
        table.appendChild(row);

        row = document.createElement("tr");
        col = document.createElement("td");
//9
        col.innerText = "Operators using this weapon";
        row.appendChild(col);
        col = document.createElement("td");
        col.innerHTML = data.operatorsHTML;

        row.appendChild(col);
        table.appendChild(row);
    });
}

jQuery(document).ready(function($) {
    getWeaponById();
});

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}