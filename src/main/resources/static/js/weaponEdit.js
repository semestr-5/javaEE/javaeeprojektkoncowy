jQuery(document).ready(function($) {
    getWeaponById()
    init();
});

function init() {
    var submit_btn = document.getElementById("submit");
    submit_btn.addEventListener("click",function(event){
        event.preventDefault();
        var data = {};
        data.weaponId = document.getElementById("id").value;
        data.name = document.getElementById("name").value;
        data.type = document.getElementById("type").value;
        data.damage = document.getElementById("damage").value;
        data.damageDrop = document.getElementById("damageDrop").value;
        data.damageSuppresed = document.getElementById("damageSuppresed").value;
        data.rateOfFire = document.getElementById("rateOfFire").value;
        data.mobility = document.getElementById("mobility").value;
        data.magazineSize = document.getElementById("magazineSize").value;
        data.ammoType = document.getElementById("ammoType").value;
        data.imageUrl = document.getElementById("imageUrl").value;

        $.ajax({
            url: "http://localhost:8080/api/weapons/edit",
            method:"PUT",
            contentType: 'application/json',
            dataType: 'json',
            data: JSON.stringify(data)
        }).done(function() {
            location.href = "http://localhost:8080/weapons/all";
        });
    });
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}


function getWeaponById() {
    var id = getParameterByName('id');
    $.ajax({
        url: "http://localhost:8080/api/weapons/id?id="+id,
    }).done(function (data) {
        document.getElementById("name").value = data.weapon.name;

        document.getElementById("type").value = data.weapon.type;
        document.getElementById("damage").value = data.weapon.damage;
        document.getElementById("damageDrop").value = data.weapon.damageDrop;
        document.getElementById("damageSuppresed").value = data.weapon.damageSuppresed;
        document.getElementById("rateOfFire").value = data.weapon.rateOfFire;
        document.getElementById("mobility").value = data.weapon.mobility;
        document.getElementById("magazineSize").value = data.weapon.magazineSize;
        document.getElementById("ammoType").value = data.weapon.ammoType;
        document.getElementById("imageUrl").value = data.weapon.imageUrl;
        document.getElementById("id").value = id;
    });
}