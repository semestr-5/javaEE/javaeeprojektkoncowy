jQuery(document).ready(function($) {
    init();
});

function init() {
    var submit_btn = document.getElementById("submit");
    submit_btn.addEventListener("click",function(event){
        event.preventDefault();
        var data = {};
        data.name = document.getElementById("name").value;
        data.type = document.getElementById("type").value;
        data.damage = document.getElementById("damage").value;
        data.damageDrop = document.getElementById("damageDrop").value;
        data.damageSuppresed = document.getElementById("damageSuppresed").value;
        data.rateOfFire = document.getElementById("rateOfFire").value;
        data.mobility = document.getElementById("mobility").value;
        data.magazineSize = document.getElementById("magazineSize").value;
        data.ammoType = document.getElementById("ammoType").value;
        data.imageUrl = document.getElementById("imageUrl").value;

        $.ajax({
            url: "http://localhost:8080/api/weapons/add",
            method:"POST",
            contentType: 'application/json',
            dataType: 'json',
            data: JSON.stringify(data)
        }).done(function() {
            location.href = "http://localhost:8080/weapons/all";
        });
    });
}