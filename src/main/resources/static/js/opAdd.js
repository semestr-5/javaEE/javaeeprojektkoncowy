jQuery(document).ready(function($) {
    init();
    getAllWeapons();
});

function init() {
    var submit_btn = document.getElementById("submit");
    submit_btn.addEventListener("click",function(event){
        event.preventDefault();
        var data = {};
        data.op={};
        data.op.firstName = document.getElementById("firstname").value;
        data.op.lastName = document.getElementById("lastname").value;
        data.op.nick = document.getElementById("nick").value;
        data.op.description = document.getElementById("description").value;
        data.op.birthDate = document.getElementById("birthdate").value;
        data.op.birthPlaceHTML = document.getElementById("birthplaceHTML").value;
        data.op.organization = document.getElementById("organization").value;
        data.op.height = document.getElementById("height").value;
        data.op.weight = document.getElementById("weight").value;
        data.op.armorRating = document.getElementById("armorrating").value;
        data.op.speedRating = document.getElementById("speedrating").value;
        data.op.mainImageUrl = document.getElementById("mainimgurl").value;
        data.op.logoUrl = document.getElementById("logourl").value;
        data.op.position = document.getElementById("position").value;
        data.weaponsId = getSelectValues();

        $.ajax({
            url: "http://localhost:8080/api/operators/add",
            method:"POST",
            contentType: 'application/json',
            dataType: 'json',
            data: JSON.stringify(data)
        }).done(function() {
            location.href = "http://localhost:8080/operators/all";
        });
    });
}

function getSelectValues() {
    var select = document.getElementById("s_weapons");
    var result = [];
    var options = select && select.options;
    var opt;

    for (var i=0, iLen=options.length; i<iLen; i++) {
        opt = options[i];

        if (opt.selected) {
            result.push(opt.value);
        }
    }
    return result;
}

function getAllWeapons() {
    $.ajax({
        url: "http://localhost:8080/api/weapons/all",
    }).done(function (data) {
        var select = document.getElementById("s_weapons");
        data.forEach(function (item) {
            var option = document.createElement("option");
            option.value=item.id;
            option.innerText=item.name;
            select.appendChild(option);
        });
    });
}