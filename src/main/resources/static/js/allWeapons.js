function getAllWeapons() {
    $.ajax({
        url: "http://localhost:8080/api/weapons/all",
    }).done(function (data) {
        data.forEach(function (item) {
            var row = document.createElement("tr");
            var col = document.createElement("td");
            var a = document.createElement("a");
            a.href="/weapons/id?id="+item.id;
            a.innerText
                = item.name;
            col.appendChild(a);
            row.appendChild(col);

            col = document.createElement("td");
            col.innerText
                = item.type;
            row.appendChild(col);


            col = document.createElement("td");
            var btn = document.createElement("a");
            btn.innerText="Edit";
            btn.classList.add("btn");
            btn.classList.add("btn-default");
            btn.href = "/weapons/edit?id=" + item.id;
            col.appendChild(btn);
            row.appendChild(col);

            col = document.createElement("td");
            btn = document.createElement("a");
            btn.innerText="Delete";
            btn.classList.add("btn");
            btn.classList.add("btn-default")
            btn.addEventListener("click",function(){
                console.log(item.id);
                deleteWeapon(item.id);
            });
            col.appendChild(btn);
            row.appendChild(col);

            document.getElementById("weapons")
                .appendChild(row);
        });
    });
}

function deleteWeapon(id){
    $.ajax({
        url: "http://localhost:8080/api/weapons/delete?id="+id,
        method:"DELETE",
    }).done(function() {
        location.reload();
    })
}

jQuery(document).ready(function($) {
    getAllWeapons();
});